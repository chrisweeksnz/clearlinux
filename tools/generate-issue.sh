#!/bin/bash

# Produce a nice issue file that is similar to that from CoreOS.

cat > /etc/issue <<'EOFISSUEPRE'

This is \n (\s \m \r) \t
EOFISSUEPRE
SSH_DIR=/etc/ssh
for KEY_FILE in $(find ${SSH_DIR} -name 'ssh_host_*_key') ; do
  ssh-keygen -l -f ${KEY_FILE}
done | awk '{print "SSH host key: " $2 " " $4}' >> /etc/issue
for nic in $(ls -1 /sys/class/net/ | grep -v -e '^lo'); do 
  echo "${nic}: \4{${nic}} \6{${nic}}"
done >> /etc/issue
echo -e '\n' >> /etc/issue